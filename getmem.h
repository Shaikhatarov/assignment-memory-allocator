#include <stdlib.h>

#ifndef __GETMEM_H_

#define __GETMEM_H_


void * getmem(size_t size);
void gaway(void *ptr);


#endif
