all: main.o getmem.o
	gcc -o main main.o getmem.o

debug: main.c getmem.c
	gcc -ggdb -o main-dbg main.c getmem.c



%.o: %.c
	gcc -c -o $@ $<

clean:
	rm *.o
	rm main
