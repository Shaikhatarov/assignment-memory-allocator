#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

#include "getmem.h"

#define ALIGNMENT 8
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~(ALIGNMENT-1))
#define MIN_BLK_SIZE ALIGN(sizeof(free_blk_header_t))
#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))

typedef struct free_blk_header {
	size_t size;
	struct free_blk_header *next;
	struct free_blk_header *prior;
} free_blk_header_t;

static size_t *heap_start_ptr = NULL;
static bool heap_start_initialized = false;

static bool sbrk_called = false;
static free_blk_header_t *heap_start_bp = NULL;
static int heap_inited = 0;
void init_heap() {
	heap_start_bp = sbrk(ALIGN(sizeof(free_blk_header_t)));
	heap_start_bp->size = 0;
	heap_start_bp->next = heap_start_bp;
	heap_start_bp->prior = heap_start_bp;
}

void *heap_end() {
   if (!sbrk_called) {
      sbrk_called = true;
      sbrk(0);
   }
	return sbrk(0);
}


void *heap_start() {
	if (!heap_start_initialized) {
        heap_start_initialized = true;
	    heap_start_ptr = heap_end();
    }
    //return heap_start_ptr;
	return heap_start_bp;
}

void *find_fit(size_t length) {

	free_blk_header_t *bp = heap_start();
	for (bp = bp->next; bp != heap_start(); bp = bp->next) {
		// find first fit
		if (bp->size >= length) {
			// remove from free list and return
			bp->next->prior = bp->prior;
			bp->prior->next = bp->next;
			return bp;
		}
	}
	return NULL;
}

void *getmem(size_t size) {
	if (!heap_inited) {
		heap_inited = 1;
		init_heap();
	}

	size_t *header;
	int blk_size = ALIGN(size + SIZE_T_SIZE);
	blk_size = (blk_size < MIN_BLK_SIZE)? MIN_BLK_SIZE : blk_size;
	header = find_fit(blk_size);
	if (header) {
		*header = ((free_blk_header_t *)header)->size | 1;
	} else {
		header = sbrk(blk_size);
		if (header == (void *)-1) {
			// errno is set by sbrk
			return (void *) -1;
		}
		*header = blk_size | 1;
	}

	return (char *)header + SIZE_T_SIZE;
}

void gaway(void *ptr) {
	free_blk_header_t *header = (void *)((char *)ptr - SIZE_T_SIZE),
	*free_list_head = heap_start();
	// add freed block to free list after head
	header->size = *(size_t *)header & ~1L;
	// add freed block to free list after head
	header->next = free_list_head->next;
	header->prior = free_list_head;
	free_list_head->next = free_list_head->next->prior = header;
}

