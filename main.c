#include <stdio.h>
#include <stdint.h>

#include "getmem.h"

struct data_block{
	uint32_t id;
	uint32_t payload;
};

struct small_db {
	uint32_t id;
};

void payload(struct data_block *ptr) {
	ptr->payload = 1024 * 1024;
}

void printf_db(struct data_block *ptr) {
	fprintf(stderr, "id: %u \t payload: %u\n", ptr->id, ptr->payload); 
}

int main(){
	

	fprintf(stderr, "\n\n-----Allocating memory-----\n\n");

	struct data_block *data1 = getmem(sizeof(struct data_block) + 1024);
	struct data_block *data2 = getmem(sizeof(struct data_block));
	struct data_block *data3 = getmem(sizeof(struct data_block));

	data1->id = 0;
	data2->id = 1;
	data3->id = 2;
	
	payload(data1);
	payload(data2);
	payload(data3);
	

	//fprintf(stderr, "\n\n-----De-allocating the memory-----\n\n");

	gaway(data1);

	struct data_block *data4 = getmem(sizeof(struct data_block));

	gaway(data2);
	struct small_db *small_data_block = getmem(sizeof(struct small_db));
	gaway(data3);
	
	printf_db(data4);

	return 0;
}
